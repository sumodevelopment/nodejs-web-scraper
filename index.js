const cheerio = require('cheerio');
const axios = require('axios');
const fs = require('fs');

const URL = 'https://gamesdb.launchbox-app.com/platforms/games/50%7C';
let currentPage = 1;
const games = [];

async function getPage() {
    
    if (currentPage > 10) {
        console.log(games);
        fs.writeFileSync('games.json', JSON.stringify( games ));
        return;
    }

    try {
        const response = await axios.get(URL + currentPage).then(r => r.data);
        extractGames(response);
        currentPage++;
        await getPage();
    } catch(e) {
        console.log(e);
    }
}

getPage();


function extractGames(response) {

    const $ = cheerio.load(response);
    
    $('.list-item-wrapper').each(function(){
        const title = $(this).find('h3');
        const image = $(this).find('img').attr('src');
        const data = $(this).find('p.sub').text();

        const dataArr = data.split('-');
        let platform, date;
        platform = dataArr[0];
        if (dataArr.length === 3) {
            date = dataArr[2];
        } else {
            date = dataArr[1];
        }
        games.push(new Game( title.text(), image, platform, date ));
    });
}

function onError(error) {
    console.error(error);
}

function Game(title, img, platform, date) {
    this.title = title;
    this.image = img;
    this.platform = platform;
    this.releaseDate = date;
}